﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Consumo_de_Jugos.ViewModels
{
    public class ReporteUsuario
    {

        public List<ReporteUsuario> ReporteTemp { get; set; }

        [Key]
        public int Clave { get; set; }

        [Display(Name = "Fecha Inicio")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "La fecha de inicio es requerida")]
        public DateTime Fecha_Inicio { get; set; }

        [Display(Name = "Fecha Fin")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "La fecha de fin es requerida")]
        public DateTime Fecha_Fin { get; set; }


        //CONTENIDO DEL REPORTE

        [Display(Name = "Nombre de Usuario")]
        public string Usuario { get; set; }

        
        [Display(Name = "Usuario")]
        [Required(ErrorMessage = "Debe elegir un Usuario")]
        public int Usuarioid { get; set; }

        [Display(Name = "Sede")]
        public string Sede { get; set; }

        [Display(Name = "Ciudad")]
        public string Ciudad { get; set; }

        [Display(Name = "Dispensador")]
        public string Maquina { get; set; }

        [Display(Name = "Jugo")]
        public string Jugo { get; set; }

        [Display(Name = "Fecha")]
        [DataType(DataType.Date)]
        public DateTime Fecha_consumo { get; set; }

        [Display(Name = "Forma de consumo")]
        public string Tipodeconsumo { get; set; }

        [Display(Name = "Observaciones")]
        public string Observaciones { get; set; }
    }
}