﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Consumo_de_Jugos.Models
{
    public class Consumo
    {

        [Key]
        public int Consumoid { get; set; }

        [Required]
        public int Usuarioid { get; set; }

        [Required]
        public int Sedeid { get; set; }


        [Required]
        public int Ciudadid { get; set; }

        [Required]
        public int Maquinaid { get; set; }

        [Required]
        public string Jugo { get; set; }

        [Required]
       
        public string FormadeConsumo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Fecha_consumo { get; set; }



        [MaxLength(800)]
        public string Observaciones { get; set; }


        public virtual Usuario Usuario { get; set; }

       
        public virtual Sede Sede { get; set; }

        public virtual Ciudad Ciudad { get; set; }

        public virtual Maquina Maquina { get; set; }



    }
}