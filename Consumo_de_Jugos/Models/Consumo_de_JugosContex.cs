﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Consumo_de_Jugos.Models
{
    public class Consumo_de_JugosContex : DbContext
    {

        public Consumo_de_JugosContex() : base("DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }


        public DbSet<Sede> Sedes { get; set; }

        public DbSet<Ciudad> Ciudads { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Consumo> Consumos { get; set; }

        public DbSet<Maquina> Maquinas { get; set; }

        
       
    }
}