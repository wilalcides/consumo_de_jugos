﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Consumo_de_Jugos.Models
{
    public class Ciudad
    {
        [Key]
        public int Ciudadid { get; set; }

        [RegularExpression("^[A-ZÑ][a-zàèìòùñ]+$", ErrorMessage = "El nombre debe tener la primer letra en mayuscula ")]
        [Display(Name ="Ciudad")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        [Index("Ciudad_Nombre_Index", IsUnique = true)]
        public string Nombre { get; set; }


        public virtual ICollection<Sede> Sedes   { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }

        public virtual ICollection<Consumo> Consumos { get; set; }
    }
}