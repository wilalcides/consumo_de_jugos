﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Consumo_de_Jugos.Models
{
    public class Sede
    {
        [Key]
        public int Sedeid { get; set; }

        [RegularExpression("^[A-ZÑ][a-zàèìòùñ]+$", ErrorMessage = "El nombre debe tener la primer letra en mayuscula")]
        [Display(Name = "Sede")]
        [Required (ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        [Index("Sede_Nombre_Index",2, IsUnique = true)]
        public string Nombre { get; set; }

        [Display(Name = "Ciudad")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}" )]
        [Index("Sede_Nombre_Index",1, IsUnique = true)]
        public int Ciudadid { get; set; }



        public virtual  Ciudad Ciudad { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }

        public virtual ICollection<Maquina> Maquinas { get; set; }

        public virtual ICollection<Consumo> Consumos { get; set; }
    }
}