﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consumo_de_Jugos.Models
{
    public class Usuario
    {
        [Key]
        public int Usuarioid { get; set; }


        [Display(Name = "E-Mail")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(256, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        [Index("Usuario_NombreUsuario_Index", IsUnique = true)]
        [DataType(DataType.EmailAddress)]
        public string NombreUsuario { get; set; }

        [Display(Name = "Nombres")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        public string Nombres { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        public string Apellidos { get; set; }

        [Display(Name = "Cedula")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        public string Documento { get; set; }

        [Display(Name = "Direccion")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        public string Direccion { get; set; }

        [Display(Name = "Ciudad")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        public int Ciudadid { get; set; }

        [Display(Name = "Sede")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [Range(1, double.MaxValue, ErrorMessage = "Debe seleccionar un {0}")]
        public int Sedeid { get; set; }

        //propiedad de lectura nos permite juntar nombr y apellidos la dejamos get para que no se mapee con la basede datos 
        // el string.format es para concatenar string
        [Display(Name = "Usuario")]
        public string FullNombre { get { return string.Format("{0} {1}", Nombres, Apellidos); } }

        public virtual Ciudad Ciudad { get; set; }

        public virtual Sede Sede { get; set; }

        public virtual ICollection <Consumo> Consumos { get; set; }

        

    }
}