﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Consumo_de_Jugos.Models
{
    public class Maquina
    {

        [Key]
        public int Maquinaid { get; set; }


        [Display(Name = "Codigo o SN")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        [MaxLength(50, ErrorMessage = "El Campo {0} debe maximo {1} caracteres")]
        [Index("Maquina_Codigo_Index", IsUnique = true)]
        public string Codigo { get; set; }

        [Display(Name = "Sede")]
        [Required(ErrorMessage = "El Campo {0} es requerido")]
        public int Sedeid { get; set; }

        [Display(Name = "Jugo Boquilla1")]      
        public string JugoB1 { get; set; }

        [Display(Name = "Jugo Boquilla 2")]        
        public string JugoB2 { get; set; }

        [Display(Name = "Jugo Boquilla 3")]       
        public string JugoB3 { get; set; }

        [Display(Name = "Jugo Boquilla 4")]       
        public string JugoB4 { get; set; }


        public virtual Sede Sede { get; set; }

        

        public virtual ICollection<Consumo> Consumos { get; set; }
    }
}