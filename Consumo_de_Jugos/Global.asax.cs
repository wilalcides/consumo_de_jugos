﻿using Consumo_de_Jugos.Clases;
using Consumo_de_Jugos.Migrations;
using Consumo_de_Jugos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Consumo_de_Jugos
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Consumo_de_JugosContex, Configuration>());
            CheckRolesAndSuperUser();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void CheckRolesAndSuperUser()
        {
            UsuarioHelper.CheckRole("Admin");
            UsuarioHelper.CheckRole("User");
            UsuarioHelper.CheckSuperUser();
        }
    }
}
