﻿using Consumo_de_Jugos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consumo_de_Jugos.Clases
{
    public class Combo_helper : IDisposable
    {
        private static Consumo_de_JugosContex db = new Consumo_de_JugosContex();

        public static List<Ciudad> GetCiudades()
        {

            //en estas lineas se organiza una lista de ciudades para un combobox 
            //logrando que la primera opcion en este sea selecione o elija y asi evitar errores del usuario 

            //se crea un objeto que nos devuelve una lista es similar a hacer una consulta selec * from
            var ciudades = db.Ciudads.ToList();

            //se agrega el campo temporal selecionar que se vera en la vista
            ciudades.Add(new Ciudad
            {
                Ciudadid = 0,
                Nombre = "[Elija la ciudad....]",
            });

            // se ordena la lista del objeto
            return ciudades.OrderBy(d => d.Nombre).ToList();


        }

        public static List<Sede> GetSedes()
        {

            //en estas lineas se organiza una lista de ciudades para un combobox 
            //logrando que la primera opcion en este sea selecione o elija y asi evitar errores del usuario 

            //se crea un objeto que nos devuelve una lista es similar a hacer una consulta selec * from
            var sedes = db.Sedes.ToList();

            //se agrega el campo temporal selecionar que se vera en la vista
            sedes.Add(new Sede
            {
                Sedeid = 0,
                Nombre = "[Elija una sede....]",
            });

            // se ordena la lista del objeto
            return sedes.OrderBy(d => d.Nombre).ToList();


        }

        public static List<Usuario> GetUsuarios()
        {

            //en estas lineas se organiza una lista de ciudades para un combobox 
            //logrando que la primera opcion en este sea selecione o elija y asi evitar errores del usuario 

            //se crea un objeto que nos devuelve una lista es similar a hacer una consulta selec * from
            var usuarios = db.Usuarios.ToList();

            //se agrega el campo temporal selecionar que se vera en la vista
            usuarios.Add(new Usuario
            {
                Usuarioid = 0,
                Nombres = "[Elija un Usuario....]",
            });

            // se ordena la lista del objeto
            return usuarios.OrderBy(d => d.Nombres).ToList();


        }

        public static List<Maquina> GetMaquinas()
        {

            //en estas lineas se organiza una lista de ciudades para un combobox 
            //logrando que la primera opcion en este sea selecione o elija y asi evitar errores del usuario 

            //se crea un objeto que nos devuelve una lista es similar a hacer una consulta selec * from
            var maquinas = db.Maquinas.ToList();

            //se agrega el campo temporal selecionar que se vera en la vista
            maquinas.Add(new Maquina
            {
                Maquinaid = 0,
                Codigo = "[Elija una maquina....]",
            });

            // se ordena la lista del objeto
            return maquinas.OrderBy(d => d.Codigo).ToList();


        }

      


      

        public void Dispose()
        {
            db.Dispose();
        }
    }
}