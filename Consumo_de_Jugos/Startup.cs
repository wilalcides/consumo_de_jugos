﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Consumo_de_Jugos.Startup))]
namespace Consumo_de_Jugos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
