﻿using System.Web;
using System.Web.Mvc;

namespace Consumo_de_Jugos
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
