namespace Consumo_de_Jugos.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Consumo_de_Jugos.Models.Consumo_de_JugosContex>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Consumo_de_Jugos.Models.Consumo_de_JugosContex";
        }

        protected override void Seed(Consumo_de_Jugos.Models.Consumo_de_JugosContex context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
