﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Consumo_de_Jugos.Clases;
using Consumo_de_Jugos.Models;

namespace Consumo_de_Jugos.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsuariosController : Controller
    {
        private Consumo_de_JugosContex db = new Consumo_de_JugosContex();

        // GET: Usuarios
        public ActionResult Index()
        {
            var usuarios = db.Usuarios.Include(u => u.Ciudad).Include(u => u.Sede);
            return View(usuarios.ToList());
        }

        // GET: Usuarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: Usuarios/Create
        public ActionResult Create()
        {
            ViewBag.Ciudadid = new SelectList(Combo_helper.GetCiudades(), "Ciudadid", "Nombre");
            ViewBag.Sedeid = new SelectList(Combo_helper.GetSedes(), "Sedeid", "Nombre");
            return View();
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                db.Usuarios.Add(usuario);
                db.SaveChanges();
                UsuarioHelper.CreateUserASP(usuario.NombreUsuario, "User");
                return RedirectToAction("Index");
            }

            ViewBag.Ciudadid = new SelectList(db.Ciudads, "Ciudadid", "Nombre", usuario.Ciudadid);
            ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", usuario.Sedeid);
            return View(usuario);
        }

        // GET: Usuarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ciudadid = new SelectList(db.Ciudads, "Ciudadid", "Nombre", usuario.Ciudadid);
            ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", usuario.Sedeid);
            return View(usuario);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Usuario usuario)
        {
            if (ModelState.IsValid)
            {

                var db2 = new Consumo_de_JugosContex();
                var usuarioActual = db2.Usuarios.Find(usuario.Usuarioid);
                if(usuarioActual.NombreUsuario != usuario.NombreUsuario)
                {

                    UsuarioHelper.Editusername(usuarioActual.NombreUsuario, usuario.NombreUsuario);

                }

                db2.Dispose();

                db.Entry(usuario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ciudadid = new SelectList(db.Ciudads, "Ciudadid", "Nombre", usuario.Ciudadid);
            ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", usuario.Sedeid);
            return View(usuario);
        }

        // GET: Usuarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = db.Usuarios.Find(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var usuario = db.Usuarios.Find(id);
            db.Usuarios.Remove(usuario);
            try
            {
                db.SaveChanges();
                UsuarioHelper.DeleteUser(usuario.NombreUsuario);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "El registro no se puede eliminar por que esta asociado a otros registros");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }

            }

            return View(usuario);

        }

        public JsonResult GetSedes (int ciudadid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var sedes = db.Sedes.Where(m => m.Ciudadid == ciudadid);
            return Json(sedes);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
