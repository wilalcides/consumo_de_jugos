﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Consumo_de_Jugos.Clases;
using Consumo_de_Jugos.Models;

namespace Consumo_de_Jugos.Controllers
{
    public class ConsumoesController : Controller
    {
        private Consumo_de_JugosContex db = new Consumo_de_JugosContex();

        // GET: Consumoes
        public ActionResult Index()
        {
            var consumos = db.Consumos.Include(c => c.Ciudad).Include(c => c.Maquina).Include(c => c.Sede).Include(c => c.Usuario);
            return View(consumos.ToList());
        }

        // GET: Consumoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumo consumo = db.Consumos.Find(id);
            if (consumo == null)
            {
                return HttpNotFound();
            }
            return View(consumo);
        }

        // GET: Consumoes/Create
        public ActionResult Create()
        {

           
            ViewBag.Usuarioid = new SelectList(Combo_helper.GetUsuarios(), "Usuarioid", "Nombres");
          
            

            
            return View();
        }

        // POST: Consumoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Consumo consumo)
        {
            if (ModelState.IsValid)
            {
                Consumo temp = new Consumo();

                temp.Ciudadid = db.Usuarios.Where(c => c.Usuarioid == consumo.Usuarioid).Select(c => c.Ciudadid).FirstOrDefault();
                temp.Sedeid = db.Usuarios.Where(c => c.Usuarioid == consumo.Usuarioid).Select(c => c.Sedeid).FirstOrDefault();
                temp.Usuarioid = consumo.Usuarioid;
                temp.Maquinaid = db.Maquinas.Where(c => c.Sedeid == temp.Sedeid).Select(c => c.Maquinaid).FirstOrDefault();
                temp.Jugo = consumo.Jugo;
                temp.FormadeConsumo = consumo.FormadeConsumo;
                temp.Observaciones = consumo.Observaciones;
                temp.Fecha_consumo = consumo.Fecha_consumo;

                db.Consumos.Add(temp);
                db.SaveChanges();
                return RedirectToAction("Create");
            }
            //ViewBag.Ciudadid = new SelectList(Combo_helper.GetCiudades(), "Ciudadid", "Nombre");
            //ViewBag.Sedeid = new SelectList(Combo_helper.GetSedes(), "Sedeid", "Nombre");
            ViewBag.Usuarioid = new SelectList(Combo_helper.GetUsuarios(), "Usuarioid", "Nombres");
           
            //ViewBag.Maquinaid = new SelectList(Combo_helper.GetMaquinas(), "Maquinaid", "Codigo");


            return View(consumo);
        }

        // GET: Consumoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumo consumo = db.Consumos.Find(id);
            if (consumo == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ciudadid = new SelectList(db.Ciudads, "Ciudadid", "Nombre", consumo.Ciudadid);
            ViewBag.Maquinaid = new SelectList(db.Maquinas, "Maquinaid", "Codigo", consumo.Maquinaid);
            ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", consumo.Sedeid);
            ViewBag.Usuarioid = new SelectList(db.Usuarios, "Usuarioid", "NombreUsuario", consumo.Usuarioid);
            return View(consumo);
        }

        // POST: Consumoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Consumoid,Usuarioid,Sedeid,Ciudadid,Maquinaid,Jugooid,FormadeConsumo,Fecha_consumo,Observaciones")] Consumo consumo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(consumo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ciudadid = new SelectList(db.Ciudads, "Ciudadid", "Nombre", consumo.Ciudadid);
            ViewBag.Maquinaid = new SelectList(db.Maquinas, "Maquinaid", "Codigo", consumo.Maquinaid);
            ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", consumo.Sedeid);
            ViewBag.Usuarioid = new SelectList(db.Usuarios, "Usuarioid", "NombreUsuario", consumo.Usuarioid);
            return View(consumo);
        }

        // GET: Consumoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consumo consumo = db.Consumos.Find(id);
            if (consumo == null)
            {
                return HttpNotFound();
            }
            return View(consumo);
        }

        // POST: Consumoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consumo consumo = db.Consumos.Find(id);
            db.Consumos.Remove(consumo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        //public JsonResult GetSedes(int usuarioid)
        //{
        //    db.Configuration.ProxyCreationEnabled = false;
        //    var temp = db.Usuarios.Where(m => m.Usuarioid== usuarioid).Select(c => c.Sedeid);
           
        //    return Json(temp);
        //}




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
