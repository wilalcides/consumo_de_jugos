﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Consumo_de_Jugos.Clases;
using Consumo_de_Jugos.Models;

namespace Consumo_de_Jugos.Controllers
{
    public class MaquinasController : Controller
    {
        private Consumo_de_JugosContex db = new Consumo_de_JugosContex();

        // GET: Maquinas
        public ActionResult Index()
        {
            var maquinas = db.Maquinas.Include(m => m.Sede);
            return View(maquinas.ToList());
        }

        // GET: Maquinas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maquina maquina = db.Maquinas.Find(id);
            if (maquina == null)
            {
                return HttpNotFound();
            }
            return View(maquina);
        }

        // GET: Maquinas/Create
        public ActionResult Create()
        {

         

            ViewBag.Sedeid = new SelectList(Combo_helper.GetSedes(), "Sedeid", "Nombre");
            return View();
        }

        // POST: Maquinas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Maquina maquina)
        {
            if (ModelState.IsValid)
            {
                db.Maquinas.Add(maquina);
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {

                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("Index"))
                    {
                        ModelState.AddModelError(string.Empty, "El registro ya existe");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "DEBE ELEGIR UNA SEDE");
                    }
                }

            }

           
            ViewBag.Sedeid = new SelectList(Combo_helper.GetSedes(), "Sedeid", "Nombre");
            return View(maquina);
            //ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", maquina.Sedeid);
            //return View(maquina);
        }

        // GET: Maquinas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maquina maquina = db.Maquinas.Find(id);
            if (maquina == null)
            {
                return HttpNotFound();
            }
         
            ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", maquina.Sedeid);
            return View(maquina);
        }

        // POST: Maquinas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Maquina maquina)
        {
            if (ModelState.IsValid)
            {
                db.Entry(maquina).State = EntityState.Modified;
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {

                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("Index"))
                    {
                        ModelState.AddModelError(string.Empty, "El registro ya existe");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "DEBE ELEGIR UNA SEDE");
                    }
                }

            }
           
            ViewBag.Sedeid = new SelectList(db.Sedes, "Sedeid", "Nombre", maquina.Sedeid);
            return View(maquina);
        }

        // GET: Maquinas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maquina maquina = db.Maquinas.Find(id);
            if (maquina == null)
            {
                return HttpNotFound();
            }
            return View(maquina);
        }

        // POST: Maquinas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Maquina maquina = db.Maquinas.Find(id);
            db.Maquinas.Remove(maquina);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
