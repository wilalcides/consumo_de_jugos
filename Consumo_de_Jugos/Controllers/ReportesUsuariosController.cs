﻿using Consumo_de_Jugos.Clases;
using Consumo_de_Jugos.Models;
using Consumo_de_Jugos.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consumo_de_Jugos.Controllers
{
    public class ReportesUsuariosController : Controller
    {

        private Consumo_de_JugosContex db = new Consumo_de_JugosContex();

        //get
        public ActionResult ReporteTemporal()
        {
            ReporteUsuario model = new ReporteUsuario() { ReporteTemp = new List<ReporteUsuario>() };

            //LoadDropDownListCond();
            ViewBag.Usuarioid = new SelectList(
               Combo_helper.GetUsuarios(), "Usuarioid", "Nombres");

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReporteTemporal(DateTime? Fecha_Inicio, DateTime? Fecha_Fin, int? usuarioid)
        {

            //ELIMINAR LOS REPORTES DE LA TABLA
            //ReporteModels rpttmp = db.Reporte.Find(reportcond);

            // var connectionString = ConfigurationManager.ConnectionStrings["ValesDBContext"].ConnectionString;
            // SqlConnection connections = new SqlConnection(connectionString);
            // SqlCommand cmd = new SqlCommand("Borrar_Reporte_conductor", connections);
            // cmd.CommandType = CommandType.StoredProcedure;
            // connections.Open();
            // cmd.ExecuteNonQuery();



            //db.Reporte.Remove(reporte);
            //db.SaveChanges();


            ReporteUsuario model = new ReporteUsuario() { ReporteTemp = new List<ReporteUsuario>() };
            if (ModelState.IsValid)
            {
                foreach (var req in db.Consumos.Where(r => (usuarioid > 0) ? r.Usuarioid == usuarioid && r.Fecha_consumo >= Fecha_Inicio && r.Fecha_consumo <= Fecha_Fin : r.Fecha_consumo >= Fecha_Inicio && r.Fecha_consumo <= Fecha_Fin).ToList())
                {

                    //ReporteConductor report = new ReporteConductor();
                    ReporteUsuario reporte = new ReporteUsuario();

                    //ReporteModels rpttmp = db.Reporte.Find(reportcond);
                    //db.Reporte.Remove(reporte);
                    //db.SaveChanges();

                    reporte.Usuario = db.Usuarios.Where(c => c.Usuarioid == req.Usuarioid).Select(c => c.Nombres).FirstOrDefault();

                    //reporte.Usuarioid = db.Usuarios.Where(c => c.Usuarioid == req.Usuarioid).Select(c => c.NombreUsuario).FirstOrDefault();
                    //report.IM = req.IM;
                    reporte.Sede = db.Sedes.Where(c => c.Sedeid == req.Sedeid).Select(c => c.Nombre).FirstOrDefault();
                    //report.CentroCostos = db.CentroCostos.Where(c => c.Clave == req.ID_CentroCostos).Select(c => c.CentroCostos).FirstOrDefault();
                    reporte.Ciudad = db.Ciudads.Where(c => c.Ciudadid == req.Ciudadid).Select(c => c.Nombre).FirstOrDefault();
                    //report.Vale = req.Vale;
                    reporte.Jugo = req.Jugo;
                    reporte.Maquina = db.Maquinas.Where(c => c.Maquinaid == req.Maquinaid).Select(c => c.Codigo).FirstOrDefault();

                    //report.Funcionario = req.Funcionario;
                    reporte.Fecha_consumo = req.Fecha_consumo;
                    // report.Fecha_Servicio = req.Fecha_Servicio;

                    reporte.Observaciones = req.Observaciones;
                    reporte.Tipodeconsumo = req.FormadeConsumo;




                    model.ReporteTemp.Add(reporte);

                    //db.Reporte.Add(reporte);
                    //db.SaveChanges();
                }
            }

            ViewBag.Usuarioid = new SelectList(
              Combo_helper.GetUsuarios(), "Usuarioid", "Nombres");
            model.ReporteTemp.OrderBy(c => c.Usuarioid);
            return View(model);
        }



        //public ActionResult PDF()
        //{
        //    var reportcond = this.GenerarReporteCond();
        //    var stream = reportcond.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(stream, "application/pdf");
        //}

        //public ActionResult XLS()
        //{
        //    var reportcond = this.GenerarReporteCond();
        //    var stream = reportcond.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
        //    return File(stream, "application/xls", "Reporte de Conductor.xls");
        //}

        //private ReportClass GenerarReporteCond()
        //{
        //    //como para el reporte no se conecta por entitiframework se debe hacer por ado
        //    var connectionString = ConfigurationManager.ConnectionStrings["ValesDBContext"].ConnectionString;
        //    var connections = new SqlConnection(connectionString);
        //    var dataTable = new DataTable();
        //    var sql = "select * from ReporteModels";

        //    try
        //    {
        //        connections.Open();
        //        var conmand = new SqlCommand(sql, connections);
        //        var adapter = new SqlDataAdapter(conmand);
        //        adapter.Fill(dataTable);

        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();

        //    }

        //    var report = new ReportClass();
        //    report.FileName = Server.MapPath("/Reportes/CrystalReportReporte Conductor.rpt");
        //    report.Load();
        //    report.SetDataSource(dataTable);
        //    return report;

        //}



        // GET: ReportesUsuarios
        public ActionResult Index()
        {
            return View();
        }
    }
}