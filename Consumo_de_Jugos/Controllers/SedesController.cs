﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Consumo_de_Jugos.Clases;
using Consumo_de_Jugos.Models;

namespace Consumo_de_Jugos.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SedesController : Controller
    {
        private Consumo_de_JugosContex db = new Consumo_de_JugosContex();

        // GET: Sedes
        public ActionResult Index()
        {
            var sedes = db.Sedes.Include(s => s.Ciudad);
            return View(sedes.ToList());
        }

        // GET: Sedes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sede sede = db.Sedes.Find(id);
            if (sede == null)
            {
                return HttpNotFound();
            }
            return View(sede);
        }

        // GET: Sedes/Create
        public ActionResult Create()
        {
           

            //con esta linea se envia la informacion al combo box
            ViewBag.Ciudadid = new SelectList(
               Combo_helper.GetCiudades(), "Ciudadid", "Nombre");
                return View();
        }

        // POST: Sedes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Sedeid,Nombre,Ciudadid")] Sede sede)
        {
            if (ModelState.IsValid)
            {
                db.Sedes.Add(sede);
                // el try cash es para hacer la validacion para mostrar el error 
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {

                    if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("Index"))
                    {
                        ModelState.AddModelError(string.Empty, "El registro ya existe");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ex.Message);
                    }
                }
                
            }

            ViewBag.Ciudadid = new SelectList(
               Combo_helper.GetCiudades(), "Ciudadid", "Nombre", sede.Ciudadid);
            return View(sede);
        }

        // GET: Sedes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sede sede = db.Sedes.Find(id);
            if (sede == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ciudadid = new SelectList(
               db.Ciudads, "Ciudadid", "Nombre", sede.Ciudadid);
            return View(sede);
        }

        // POST: Sedes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Sedeid,Nombre,Ciudadid")] Sede sede)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sede).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Ciudadid = new SelectList(db.Ciudads, "Ciudadid", "Nombre", sede.Ciudadid);
            return View(sede);
        }

        // GET: Sedes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sede sede = db.Sedes.Find(id);
            if (sede == null)
            {
                return HttpNotFound();
            }
            return View(sede);
        }

        // POST: Sedes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sede sede = db.Sedes.Find(id);
            db.Sedes.Remove(sede);
            try
            {
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    ModelState.AddModelError(string.Empty, "El registro no se puede eliminar por que esta asociado a otros registros");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }

            }

            return View(sede);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
